﻿Imports System.Text
Imports GeneratePrimes
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class GeneratePrimesTest

    <TestMethod()> Public Sub TestPrimes()
        Dim nullArray() As Integer = PrimeGenerator.GeneratePrimes(0)
        Assert.IsNull(nullArray)

        Dim minArray() As Integer = PrimeGenerator.GeneratePrimes(2)
        Assert.AreEqual(1, minArray.Length)
        Assert.AreEqual(2, minArray(0))

        Dim threeArray() As Integer = PrimeGenerator.GeneratePrimes(3)
        Assert.AreEqual(2, threeArray.Length)
        Assert.AreEqual(2, threeArray(0))
        Assert.AreEqual(3, threeArray(1))

        Dim centArray() As Integer = PrimeGenerator.GeneratePrimes(100)
        Assert.AreEqual(25, centArray.Length)
        Assert.AreEqual(97, centArray(24))
    End Sub

    <TestMethod> Public Sub TestExhaustive()
        For i = 2 To 500
            VerifyPrimeList(PrimeGenerator.GeneratePrimes(i))
        Next
    End Sub

    Private Sub VerifyPrimeList(primes() As Integer)
        For Each prime As Integer In primes
            VerifyPrime(prime)
        Next
    End Sub

    Private Sub VerifyPrime(prime As Integer)
        For factor = 2 To prime - 1
            Assert.IsTrue(prime Mod factor <> 0)
        Next

    End Sub
End Class