﻿''' <summary>
''' このクラスは素数を生成する。生成される素数の上限はユーザが指定する。
''' ここで使用しているアルゴリズムは、「エラトステネスのふるい」法である。
''' このアルゴリズムは極めて単純である。
''' 2から始まる整数配列を与え、2の倍数を全て消す。
''' まだ消えていない次の整数を見つけ、その倍数を全て削除する。
''' 一番大きい数の平方根を超えるまで、この作業を繰り返す。
''' </summary>
Public Class PrimeGenerator
    ''' <summary>素数判定配列</summary>
    Private Shared _crossedOut() As Boolean
    ''' <summary>結果配列</summary>
    Private Shared _result() As Integer

    ''' <summary>
    ''' 素数を生成します。
    ''' </summary>
    ''' <param name="maxValue">素数の上限値</param>
    ''' <returns>素数の配列</returns>
    Public Shared Function GeneratePrimes(maxValue As Integer) As Integer()
        If maxValue < 2 Then Return Nothing
        UncrossIntegersUpTo(maxValue)
        CrossOutMultiples()
        PutUncrossedIntegersIntoResult()
        Return _result
    End Function

    ''' <summary>
    ''' 数値配列の初期化
    ''' </summary>
    ''' <param name="maxValue">素数の上限値</param>
    Private Shared Sub UncrossIntegersUpTo(maxValue As Integer)
        ' 宣言
        ReDim _crossedOut(maxValue)
        Dim i As Integer

        ' 配列を真(True)に初期化
        _crossedOut(0) = True
        _crossedOut(1) = True
        For i = 2 To _crossedOut.Length - 1 Step 1
            _crossedOut(i) = False
        Next
    End Sub

    ''' <summary>
    ''' 倍数を削除する(ふるい落とす)。
    ''' </summary>
    Private Shared Sub CrossOutMultiples()
        Dim maxPrimeFactor = DeetermineIterationLimit()

        ' ふるい落とす
        Dim i As Integer
        Dim j As Integer
        For i = 2 To maxPrimeFactor Step 1
            If IsNotCrossed(i) Then ' iがのぞかれていなければ、その倍数を除く
                CrossOutMultiplesOf(i)
            End If
        Next
    End Sub

    ''' <summary>
    ''' 繰り返しの上限値を決定する。
    ''' </summary>
    ''' <returns>繰り返しの上限値</returns>
    Private Shared Function DeetermineIterationLimit() As Integer
        ' 配列に格納されているいかなる倍数も、その配列サイズの平方根に等しいか、
        ' それよりも小さい素数因子を持っている。したがって、
        ' その平方根よりも大きな数の倍数をチェックする必要はない。
        Dim maxPrimeFactor As Decimal = Math.Sqrt(_crossedOut.Length - 1) + 1
        Return maxPrimeFactor
    End Function

    ''' <summary>
    ''' 倍数でない
    ''' </summary>
    ''' <param name="i">対象のインデックス</param>
    ''' <returns></returns>
    Private Shared Function IsNotCrossed(i As Integer) As Boolean
        Return _crossedOut(i) = False
    End Function

    ''' <summary>
    ''' 倍数を外す
    ''' </summary>
    ''' <param name="i">対象のインデックス</param>
    Private Shared Sub CrossOutMultiplesOf(i As Integer)
        For j = 2 * i To _crossedOut.Length - 1 Step i
            _crossedOut(j) = True
        Next
    End Sub

    ''' <summary>
    ''' ふるいの結果を配列に設定
    ''' </summary>
    Private Shared Sub PutUncrossedIntegersIntoResult()
        ReDim _result(GetNumberOfUncrossedIntegers() - 1)

        ' 素数の抜き出し
        Dim j As Integer = 0
        For i = 0 To _crossedOut.Length - 1 Step 1
            If IsNotCrossed(i) Then
                _result(j) = i
                j += 1
            End If
        Next
    End Sub

    ''' <summary>
    ''' 倍数でないものの件数を取得する
    ''' </summary>
    ''' <returns>倍数でないものの件数</returns>
    Private Shared Function GetNumberOfUncrossedIntegers()
        Dim count As Integer = 0
        For i = 2 To _crossedOut.Length - 1 Step 1
            If IsNotCrossed(i) Then ' 素数であれば
                count += 1
            End If
        Next
        Return count
    End Function

End Class
