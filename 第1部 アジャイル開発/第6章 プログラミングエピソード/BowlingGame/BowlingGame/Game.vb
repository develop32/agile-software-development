﻿Public Class Game
    Private _currentFrame As Integer = 0
    Private _firstThrowInFrame As Boolean = True
    Private _scorer As New Scorer


    Public ReadOnly Property Score As Integer
        Get
            Return _scorer.ScoreForFrame(_currentFrame)
        End Get
    End Property

    Public Sub Add(ByVal pins As Integer)
        _scorer.AddThrow(pins)
        AdjustCurrentFrame(pins)
    End Sub

    Private Sub AdjustCurrentFrame(pins As Integer)

        If LastBallInFrame(pins) Then
            AdvanceFrame()
            _firstThrowInFrame = True
        Else
            _firstThrowInFrame = False
        End If
    End Sub

    Private Function LastBallInFrame(pins As Integer) As Boolean
        Return Strike(pins) OrElse Not _firstThrowInFrame
    End Function

    Private Function Strike(pins As Integer) As Boolean
        Return _firstThrowInFrame AndAlso pins = 10
    End Function


    Private Sub AdvanceFrame()
        _currentFrame = Math.Min(10, _currentFrame + 1)
    End Sub

    Public Function ScoreForFrame(theFrame As Integer) As Integer
        Return _scorer.ScoreForFrame(theFrame)
    End Function

End Class
