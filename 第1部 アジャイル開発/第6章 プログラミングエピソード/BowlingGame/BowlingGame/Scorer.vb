﻿Public Class Scorer
    Private _ball As Integer
    Private ReadOnly _throws As New List(Of Integer)
    Private _currentThrow As Integer = 0

    Sub AddThrow(pins As Integer)
        _currentThrow += 1
        _throws.Insert(_currentThrow - 1, pins)
    End Sub
    Public Function ScoreForFrame(theFrame As Integer) As Integer
        _ball = 0
        Dim score As Integer = 0

        For currentFrame As Integer = 0 To theFrame - 1
            If IsStrike() Then
                score += 10 + NextTwoBallsForStrike()
                _ball += 1
            ElseIf IsSpare() Then
                score += 10 + NextBallForSpare()
                _ball += 2
            Else
                score += TwoBallsInFrame()
                _ball += 2
            End If
        Next

        Return score
    End Function
    Private Function IsStrike()
        Return _throws(_ball) = 10
    End Function

    Private Function IsSpare()
        Return _throws(_ball) + _throws(_ball + 1) = 10
    End Function

    Private Function NextTwoBallsForStrike() As Integer
        Return _throws(_ball + 1) + _throws(_ball + 2)
    End Function
    Private Function NextBallForSpare() As Integer
        Return _throws(_ball + 2)
    End Function
    Private Function TwoBallsInFrame() As Integer
        Return _throws(_ball) + _throws(_ball + 1)
    End Function
End Class
