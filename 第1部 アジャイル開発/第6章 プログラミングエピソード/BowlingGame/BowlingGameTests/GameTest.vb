﻿Imports System.Text
Imports BowlingGame
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class GameTest
    Private _target As New Game
    <TestInitialize> Public Sub 前準備()
        _target = New Game
    End Sub

    <TestMethod()> Public Sub 二回投球し5本と4本倒した場合スコアが9であること()
        _target.Add(5)
        _target.Add(4)
        Assert.AreEqual(9, _target.Score)
    End Sub

    <TestMethod()> Public Sub 四回投球し5本と4本と7本と2本倒した場合スコアが18であること()
        _target.Add(5)
        _target.Add(4)
        _target.Add(7)
        _target.Add(2)
        Assert.AreEqual(18, _target.Score)
        Assert.AreEqual(9, _target.ScoreForFrame(1))
        Assert.AreEqual(18, _target.ScoreForFrame(2))
    End Sub

    <TestMethod> Public Sub シンプルスペア()
        _target.Add(3)
        _target.Add(7)
        _target.Add(3)
        Assert.AreEqual(13, _target.ScoreForFrame(1))
    End Sub

    <TestMethod()> Public Sub 四回投球し3本と7本と3本倒した場合1フレーム目のスコアが13であること()
        _target.Add(3)
        _target.Add(7)
        _target.Add(3)
        _target.Add(2)
        Assert.AreEqual(13, _target.ScoreForFrame(1))
        Assert.AreEqual(18, _target.ScoreForFrame(2))
        Assert.AreEqual(18, _target.Score)
    End Sub

    <TestMethod> Public Sub 単純なストライク()
        _target.Add(10)
        _target.Add(3)
        _target.Add(6)
        Assert.AreEqual(19, _target.ScoreForFrame(1))
        Assert.AreEqual(28, _target.ScoreForFrame(2))
        Assert.AreEqual(28, _target.Score)
    End Sub

    <TestMethod> Public Sub パーフェクトゲーム()
        For i As Integer = 1 To 12
            _target.Add(10)
        Next
        Assert.AreEqual(300, _target.Score)
    End Sub


    <TestMethod> Public Sub 四回投球マーク無し無し()
        _target.Add(5)
        _target.Add(4)
        _target.Add(7)
        _target.Add(2)
        Assert.AreEqual(18, _target.Score)
        Assert.AreEqual(9, _target.ScoreForFrame(1))
        Assert.AreEqual(18, _target.ScoreForFrame(2))
    End Sub

    <TestMethod> Public Sub 二回投球マーク無し無し()
        _target.Add(5)
        _target.Add(4)
        Assert.AreEqual(9, _target.Score)
    End Sub



    <TestMethod> Public Sub スペア後のシンプルフレーム()
        _target.Add(3)
        _target.Add(7)
        _target.Add(3)
        _target.Add(2)
        Assert.AreEqual(13, _target.ScoreForFrame(1))
        Assert.AreEqual(18, _target.ScoreForFrame(2))
        Assert.AreEqual(18, _target.Score)
    End Sub


    <TestMethod> Public Sub 配列の終わり()
        For i As Integer = 0 To 8
            _target.Add(0)
            _target.Add(0)
        Next
        _target.Add(2)
        _target.Add(8)
        _target.Add(10)
        Assert.AreEqual(20, _target.Score)
    End Sub

    <TestMethod> Public Sub サンプルゲーム()
        _target.Add(1)
        _target.Add(4)

        _target.Add(4)
        _target.Add(5)

        _target.Add(6)
        _target.Add(4)

        _target.Add(5)
        _target.Add(5)

        _target.Add(10)

        _target.Add(0)
        _target.Add(1)

        _target.Add(7)
        _target.Add(3)

        _target.Add(6)
        _target.Add(4)

        _target.Add(10)

        _target.Add(2)
        _target.Add(8)
        _target.Add(6)

        Assert.AreEqual(133, _target.Score)
    End Sub

    <TestMethod> Public Sub ハートブレイク()
        For i = 1 To 11
            _target.Add(10)
        Next
        _target.Add(9)

        Assert.AreEqual(299, _target.Score)
    End Sub

    <TestMethod> Public Sub 十フレームスペア()
        For i = 1 To 9
            _target.Add(10)
        Next
        _target.Add(9)
        _target.Add(1)
        _target.Add(1)

        Assert.AreEqual(270, _target.Score)
    End Sub


End Class